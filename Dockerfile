FROM ruby:3.0.0

# 必要最低限のツールを入れる
RUN apt-get update -qq && apt-get install -y sqlite3 vim nodejs npm sudo

RUN gem install bundler
RUN gem update bundler

RUN mkdir bootstrap-dev-preview

# アプリケーションディレクトリを作業用ディレクトリに設定
WORKDIR /bootstrap-dev-preview
ADD . .
RUN bundle install
EXPOSE 3000
