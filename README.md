# Bootstrap::Dev::Preview
- This Gem Helps Web Developerw using Bootstrap.

## Installation
```ruby
gem 'bootstrap-dev-preview'
```

```bash
$ bundle install
```

## Usage
## Development

Add this medhod to your view file.

```erb
<%= (Bootstrap::Dev::Preview.hi).html_safe %>
```

## Contributing
- I will be happy to get idea from you!
- [Here](https://gitlab.com/share292508/bootstrap-dev-preview/-/issues/new) is issue creation

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).


## Contribute
- How to Ready to start develop?
  - [This text](docs/Develop/README.md) will support you to create environment.