## Build
```bash
$ docker compose up -d
$ docker compose ps
```

## Test
```bash
$ docker compose exec ruby bash

$ docker compose exec ruby bundle exec rspec spec
```

## Check
- Build and Excute method at own irb.
```bash
$ docker compose exec ruby rake build
$ docker compose exec ruby irb
irb $ require_relative "lib/bootstrap/dev/preview"
irb $ Bootstrap::Dev::Preview.hi
```