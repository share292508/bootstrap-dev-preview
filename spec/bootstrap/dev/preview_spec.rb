# frozen_string_literal: true

RSpec.describe Bootstrap::Dev::Preview do
  it "First Test" do
    expect(true).to be true
  end

  it "Return object has Bootstrap Basic Colors" do
    expect(Bootstrap::Dev::Preview.hi).to match(/'primary|secondary|info|warning|dark|light|white/)
  end

  it "BVP section has an Explain text " do
    expect(Bootstrap::Dev::Preview.hi)
    .to match(
      /This is Bootstrap CSS Preview Section./
    )
  end
end