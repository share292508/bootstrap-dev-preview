# frozen_string_literal: true

require_relative "preview/version"

module Bootstrap
  module Dev
    module Preview
      def self.hi
        # bootstrap basic defined colors
        colors =  ['primary','secondary','info','warning','dark','light','white']
        returned = "<h5>This is Bootstrap CSS Preview Section.<h5>"
        colors.each do |color|
          returned += "<p class='bg-#{color} p-3'>This is #{color} color</p>"
        end
        returned
      end
    end
  end
end
