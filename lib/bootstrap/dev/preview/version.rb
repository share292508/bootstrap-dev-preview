# frozen_string_literal: true

module Bootstrap
  module Dev
    module Preview
      VERSION = "1.0.2"
    end
  end
end
